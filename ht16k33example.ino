#include <Wire.h>
#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"
#include "SimplexNoise.h"
  // https://github.com/jshaw/SimplexNoise

#define DISPLAY_ADDRESS 0x70

#ifndef _BV
  #define _BV(bit) (1<<(bit))
#endif

SimplexNoise sn;

float increase = 0.001;
float x = 0.0;
float y = 0.0;

Adafruit_AlphaNum4 disp = Adafruit_AlphaNum4();

String default_text = "   /  GPN-22  COMMON CODE()  DIFFERENT ROOTS   ";
String text_to_display = default_text;
//String text_to_display = "0123456789";
int string_pointer = 0;
char display_buffer[24];
int brightness = 4;
bool reverse = false;
unsigned int displays[6];
int step_time = 24;
int step = step_time;
unsigned int default_waiting_time = 123;
unsigned int waiting_time = default_waiting_time;
bool random_mode = false;
bool moo = false;
bool party_mode = false;
unsigned int current_round = 0;
unsigned int max_rounds = 234;


void setup() {
  displays[0] = 0x75;
  displays[1] = 0x74;
  displays[2] = 0x73;
  displays[3] = 0x72;
  displays[4] = 0x71;
  displays[5] = 0x70;

  for (int d = 0; d < 6; d++) {
    disp.begin(displays[d]);
    disp.setBrightness(brightness);
    disp.clear();
    disp.writeDisplay();  
  }

  Serial.begin(9600);
}

void show_star(int display, int pos) {
  int wait_time = 67;

  disp.begin(displays[display]);
  int row = 8; // off
  int segment = 0;
  disp.displaybuffer[pos] = _BV((row+segment) % 16);
  disp.writeDisplay();
  delay(wait_time);
  disp.displaybuffer[pos] = _BV((row+segment) % 16) | _BV((row+1) % 16);
  disp.writeDisplay();
  delay(wait_time);
  disp.displaybuffer[pos] = _BV((row+segment) % 16) | _BV((row+1) % 16) | _BV((10) % 16);
  disp.writeDisplay();
  delay(wait_time);
  disp.displaybuffer[pos] = _BV((row+segment) % 16) | _BV((row+1) % 16) | _BV((10) % 16) | _BV((13) % 16);
  disp.writeDisplay();
  delay(wait_time);
  disp.displaybuffer[pos] = _BV((row+segment) % 16) | _BV((row+1) % 16) | _BV((10) % 16)
    | _BV((13) % 16) | _BV((12) % 16);
  disp.writeDisplay();
  delay(wait_time);
  disp.displaybuffer[pos] = _BV((row+segment) % 16) | _BV((row+1) % 16) | _BV((10) % 16)
    | _BV((13) % 16) | _BV((12) % 16) | _BV((11) % 16);
  disp.writeDisplay();
  delay(wait_time * 2);
}

void loop() {
  if (moo) {
    for (int p = 0; p < 4; p++) {
      for (int d = 0; d < 6; d++) {
        show_star(d, p);
      }
    }

    delay(2342);
    moo = false;
  }
  else if (random_mode) {
    int display = random(6);
    disp.begin(displays[display]);
    
    double loops_raw = (sn.noise(x, y) + 1) * 2 + 2;
    int loops = (int) loops_raw;
    x += increase;
    for (int l = 1; l < loops; l++) {
      int row = random(8);
      int segment = random(16);
      int row2 = random(8);
      int segment2 = random(16);
      disp.displaybuffer[row] = _BV((row+segment) % 16) | _BV((row2+segment2+8) % 16);
    }
    
    brightness = random(10);
    disp.setBrightness(brightness);

    disp.writeDisplay();
  }
  else {
    if (string_pointer >= text_to_display.length()) {
      string_pointer = 0;
    }

    // move the existing characters one position to the left
    for (int u = 0; u < 23; u++) {
      display_buffer[u] = display_buffer[u + 1];
    }

    // replace the right-most character with the next
    // character from the text_to_display variable
    string_pointer++;
    display_buffer[23] = text_to_display.charAt(string_pointer);
    
    for (int d = 0; d < 6; d++) {
      disp.begin(displays[d]);

      // set brightness every step time
      if (step == 0) {
        if (brightness > 10) {
          reverse = true;
        }
        if (brightness < 1) {
          reverse = false;
        }
        if (reverse) {
          brightness--;
        }
        else {
          brightness++;
        }  
        step = step_time;
      }
      else {
        step--;
      }

      disp.setBrightness(brightness);

      // send the text to the current display
      for (int i = 0; i < 4; i++) {
        disp.writeDigitAscii(i, display_buffer[i+(d*4)]);
      }

      // display the text
      disp.writeDisplay();
    }    
  }

  if (party_mode) {
    if (current_round == max_rounds) {
      unsigned int mode = random(3);
      if (mode == 0) {
        text_to_display = default_text;
        random_mode = false;
        waiting_time = default_waiting_time;
      }
      else if (mode == 1) {
        random_mode = true;
        waiting_time = 12;
      }
      else if (mode == 2) {
        moo = true;
      }
      Serial.print("mode: ");
      Serial.println(mode);
    }
  }

  current_round++;
  if (current_round > max_rounds) {
    current_round = 0;
  }

  // update again after waiting time
  delay(waiting_time);

  // change text via serial
  if (Serial.available() > 0) {
    String new_text = Serial.readString();

    if (new_text.startsWith(">")) {
      String command = new_text.substring(1);
      if (command == "gpn") {
        text_to_display = default_text;
        party_mode = false;
        random_mode = false;
        waiting_time = default_waiting_time;
      }
      else if (command == "rnd") {
        party_mode = false;
        random_mode = true;
        waiting_time = 12;
      }
      else if (command == "moo") {
        moo = true;
      }
      else if (command == "prt") {
        party_mode = true;
        Serial.println("party_mode");
      }
    }
    else {
      new_text.toUpperCase();
      text_to_display = " ";
      text_to_display.concat(new_text);
      text_to_display.concat(" ");
    }

    // debug: prints the received data
    Serial.print("new text: ");
    Serial.println(text_to_display);
  }
}
